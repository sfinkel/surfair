import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';
import app from './containers/AppContainer/reducers';

const rootReducer = combineReducers({
  routing: routerReducer,
  app,
});

export default rootReducer;
