const zipRegex = /^\d{5}$/;

export default (value) =>
  value && zipRegex.test(value) ? undefined : 'Please enter the 5-digit zip';
