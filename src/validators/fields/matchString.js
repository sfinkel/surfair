export default (string) => (value) =>
  value && value !== string ? `Please enter "${string}"` : undefined;
