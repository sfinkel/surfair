export default (value) => (value >= 0 ? undefined : 'Invalid amount');
