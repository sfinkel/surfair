export default (value) => (value || value === 0 ? undefined : 'Required');
