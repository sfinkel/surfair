export default (min) => (value) =>
  value && value.length < min
    ? `May not be less than ${min} characters`
    : undefined;
