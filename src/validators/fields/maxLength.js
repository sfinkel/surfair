export default (errorMessage, maxLength) => (value) =>
  value && value.length > maxLength ? errorMessage : undefined;
