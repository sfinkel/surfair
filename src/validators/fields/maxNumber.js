export default (errorMessage, maxNumber = Number.MAX_SAFE_INTEGER) => (value) =>
  value && value > maxNumber ? errorMessage : undefined;
