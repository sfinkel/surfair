export default (errorMessage) => (value) =>
  value || value === 0 ? undefined : errorMessage;
