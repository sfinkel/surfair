export default (errorMessage, minimumNumber = 0) => (value) =>
  (value || value === 0) && value < minimumNumber ? errorMessage : undefined;
