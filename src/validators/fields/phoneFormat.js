// TODO come back and fix this
const phoneRegex = /^[0,2-9]\d{2}-[0,2-9]\d{2}-\d{4}$/;

export default (value) =>
  value && phoneRegex.test(value)
    ? undefined
    : 'Please enter a valid phone number';
