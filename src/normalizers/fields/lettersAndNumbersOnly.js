const nonDigitsAndLetters = new RegExp(/[^a-zA-Z0-9]/g);

export default (value) => value && value.replace(nonDigitsAndLetters, '');
