const nonLetters = new RegExp(/[^a-zA-Z]/g);

export default (value) => value && value.replace(nonLetters, '');
