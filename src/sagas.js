import { all, fork } from 'redux-saga/effects';
import appSaga from './containers/AppContainer/sagas';

export default function* rootSaga() {
  yield all([...appSaga].map(fork));
}
