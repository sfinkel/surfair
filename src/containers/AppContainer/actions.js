import {
  GET_DATA,
  GET_DATA_FAIL,
  GET_DATA_SUCCESS,
  ADD_QUOTE,
  ADD_QUOTE_FAIL,
  ADD_QUOTE_SUCCESS,
} from './constants';

export const getData = (data) => ({
  type: GET_DATA,
  payload: data,
});

export const getDataFail = (message) => ({
  type: GET_DATA_FAIL,
  payload: message,
});

export const getDataSuccess = (data) => ({
  type: GET_DATA_SUCCESS,
  payload: data,
});

export const addQuote = (data) => ({
  type: ADD_QUOTE,
  payload: data,
});

export const addQuoteFail = (message) => ({
  type: ADD_QUOTE_FAIL,
  payload: message,
});

export const addQuoteSuccess = (data) => ({
  type: ADD_QUOTE_SUCCESS,
  payload: data,
});
