import { createSelector } from 'reselect';

const getAppData = (state = {}, props) => {
  const { app: { symbols = {}, isLoading = false, quotes = [] } = {} } = state;
  return { isLoading, symbols, quotes };
};

export const appData = createSelector(getAppData, (data) => data);

export const makeAppData = () => {
  return createSelector([getAppData], (data) => data);
};
