import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Switch, Router } from 'react-router-dom';
import { withRouter } from 'react-router';
// import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import { Grid } from '@material-ui/core';
import { HomeContainer } from '../../containers';
import { Layout, RouteWithLayout, Header, NotFound } from '../../components';
import * as actions from './actions';
import styles from './styles';

const Routes = (props) => {
  return (
    <Grid container>
      <Switch>
        <RouteWithLayout
          exact
          path="/"
          layout={Layout}
          component={HomeContainer}
        />
        <RouteWithLayout component={NotFound} layout={Layout} />
      </Switch>
    </Grid>
  );
};
class AppContainer extends Component {
  componentDidMount() {
    this.props.getData();
  }

  render() {
    return (
      <Grid container>
        <Header enableIconButton={false} />
        <Router {...this.props}>
          <Routes />
        </Router>
      </Grid>
    );
  }
}

// Using memoized selectors
// const makeMapStateToProps = () => {
//   return state;
// };

AppContainer = withStyles(styles)(AppContainer);
export default withRouter(
  connect(
    (state) => state,
    { ...actions },
  )(AppContainer),
);
