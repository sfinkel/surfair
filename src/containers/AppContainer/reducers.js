import { mapKeys } from 'lodash';
import initialState from '../../initialState';
import {
  GET_DATA,
  GET_DATA_FAIL,
  GET_DATA_SUCCESS,
  ADD_QUOTE,
  ADD_QUOTE_SUCCESS,
  ADD_QUOTE_FAIL,
} from './constants';
export default function appContainerReducer(
  state = initialState,
  { type, payload = {} },
) {
  switch (type) {
    case GET_DATA:
      return { ...state, symbols: payload, isLoading: true };
    case GET_DATA_FAIL:
      return state;
    case GET_DATA_SUCCESS:
      return { ...state, symbols: mapKeys(payload) };
    case ADD_QUOTE:
      return state;
    case ADD_QUOTE_FAIL:
      console.error(payload);
      return state;
    case ADD_QUOTE_SUCCESS:
      return { ...state, quotes: [...state.quotes, payload] };
    default:
      return state;
  }
}
