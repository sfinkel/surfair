export const GET_DATA = 'AppContainer/GET_DATA';
export const GET_DATA_SUCCESS = 'AppContainer/GET_DATA_SUCCESS';
export const GET_DATA_FAIL = 'AppContainer/GET_DATA_FAIL';
export const LOADING = 'AppContainer/LOADING';
export const ADD_QUOTE = 'AppContainer/ADD_QUOTE';
export const ADD_QUOTE_SUCCESS = 'AppContainer/ADD_QUOTE_SUCCESS';
export const ADD_QUOTE_FAIL = 'AppContainer/ADD_QUOTE_FAIL';
