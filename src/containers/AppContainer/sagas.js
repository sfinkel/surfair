import { fetchInitialData, fetchQuoteData } from '../../api';
import { getDataFail, getDataSuccess, addQuoteSuccess } from './actions';
import { GET_DATA, ADD_QUOTE } from './constants';
import { call, put, takeLatest } from 'redux-saga/effects';

export function* getData() {
  try {
    const data = yield call(fetchInitialData);
    yield put(getDataSuccess(data));
  } catch (message) {
    yield put(getDataFail(message));
  }
}

export function* addQuote({ payload }) {
  try {
    const [data] = yield call(fetchQuoteData, payload);
    yield put(addQuoteSuccess(data));
  } catch (message) {
    yield put(getDataFail(message));
  }
}

export function* getDataWatcher() {
  yield takeLatest(GET_DATA, getData);
}

export function* addQuoteWatcher() {
  yield takeLatest(ADD_QUOTE, addQuote);
}

export default [getDataWatcher, addQuoteWatcher];
