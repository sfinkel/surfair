export default (theme) => ({
  card: {
    title: {
      textAlign: 'center',
      margin: '0 auto',
    },
    paddingTop: theme.spacing.unit,
    paddingRight: theme.spacing.unit,
    paddingLeft: theme.spacing.unit,
    paddingBottom: theme.spacing.unit,
  },
  input: {
    width: `calc(100% - ${16}px)`,
  },
  quoteBox: {
    marginBottom: '20px',
  },
  root: {
    //maxWidth: 1020,
    //margin: '55px auto 0 auto',
    flexGrow: 1,
    paddingTop: theme.spacing.unit * 3,
    paddingRight: theme.spacing.unit * 3,
    paddingLeft: theme.spacing.unit * 3,
    paddingBottom: theme.spacing.unit * 3,
  },
  submitButton: {
    margin: theme.spacing.unit,
    backgroundColor: '#006EFF',
    borderColor: '#006EFF',
    borderRadius: 100,
    boxShadow: 'none',
    color: '#FFFFFF',
    float: 'right',
    fontSize: 13,
    '&:hover': {
      backgroundColor: '#0069d9',
      borderColor: '#0062cc',
    },
    '&:active': {
      boxShadow: 'none',
      backgroundColor: '#0062cc',
      borderColor: '#005cbf',
    },
    '&:focus': {
      boxShadow: '0 0 0 0.2rem rgba(0,123,255,.5)',
    },
  },
});
