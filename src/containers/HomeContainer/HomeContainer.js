import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import { Grid, Typography } from '@material-ui/core';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import { values } from 'lodash';
import { makeAppData } from '../AppContainer/selectors';
import * as actions from '../AppContainer/actions';
import { SimpleList } from '../../components';
import styles from './styles';

class HomeContainer extends Component {
  onClickRow = (id, value) => {
    //return this.props.history.push(`/lists/${status}/report/${id}`);
  };

  handleClick = (ev, val) => {
    this.props.addQuote(val);
    //return this.props.history.push(`/quote/${val}`);
  };

  render() {
    const { classes, symbols, quotes = [], isLoading } = this.props;
    return (
      <Grid
        container
        className={classes.root}
        justify="space-between"
        spacing={16}
      >
        {!isLoading ? (
          <Grid container>
            <Grid item xs={12} sm={6} md={6} lg={6}>
              <Typography variant="caption">Symbols</Typography>
              <SimpleList data={symbols} onClick={this.handleClick} />
            </Grid>
            <Grid item xs={12} sm={6} md={6} lg={6}>
              <Typography variant="caption">Quotes</Typography>
              {quotes
                ? quotes.map((quoteData, idx) => {
                    return (
                      <Grid
                        item
                        xs={12}
                        sm={6}
                        md={6}
                        lg={6}
                        key={idx}
                        className={classes.quoteBox}
                      >
                        <SimpleList
                          data={values(quoteData)}
                          onClick={this.handleClick}
                        />
                      </Grid>
                    );
                  })
                : null}
            </Grid>
          </Grid>
        ) : (
          <div>loading...</div>
        )}
      </Grid>
    );
  }
}

//Using memoized selectors
const makeMapStateToProps = () => {
  const getAppData = makeAppData();
  return (state, props) => {
    const { symbols, quotes } = getAppData(state, props);
    return { symbols, quotes };
  };
};

HomeContainer = withStyles(styles)(HomeContainer);
// export default HomeContainer;
export default withRouter(
  connect(
    makeMapStateToProps,
    { ...actions },
  )(HomeContainer),
);
