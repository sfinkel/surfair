import React from 'react';
import { defaultTo } from 'lodash';
import {
  FormControl,
  FormLabel,
  FormHelperText,
  RadioGroup,
} from '@material-ui/core';

const RadioButtonsGroup = ({
  id,
  label,
  helpText,
  horizontal = true,
  children,
  input,
  meta,
  ...rest
}) => {
  const { touched, error } = defaultTo(meta, {});
  const helpTextToDisplay = touched && Boolean(error) ? error : helpText;

  return (
    <FormControl component="fieldset" error={touched && Boolean(error)}>
      {label && <FormLabel component="legend">{label}</FormLabel>}
      <RadioGroup
        id={id}
        row={horizontal}
        onChange={(event, value) => input.onChange(value)}
        {...input}
        {...rest}
      >
        {children}
      </RadioGroup>
      {helpTextToDisplay && (
        <FormHelperText>{helpTextToDisplay}</FormHelperText>
      )}
    </FormControl>
  );
};

export default RadioButtonsGroup;
