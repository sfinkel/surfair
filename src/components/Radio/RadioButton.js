import React from 'react';
import { Radio, Icon } from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';
import { RadioButtonUnchecked, RadioButtonChecked } from '@material-ui/icons';

const RadioButton = ({ classes, color, ...rest }) => (
  <Radio
    {...rest}
    className={classes.root}
    color={color}
    icon={
      <Icon style={{ fontSize: 'inherit' }} color="inherit">
        <RadioButtonUnchecked />
      </Icon>
    }
    checkedIcon={
      <Icon style={{ fontSize: 'inherit' }} color="inherit">
        <RadioButtonChecked />
      </Icon>
    }
  />
);

//TODO export these styles
const styles = (theme) => ({
  root: {
    fontSize: 32,
  },
});

export default withStyles(styles)(RadioButton);
