import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';

const styles = (theme) => ({
  root: {
    width: '100%',
    maxWidth: 360,
    backgroundColor: theme.palette.background.paper,
  },
});

class SimpleMenu extends React.Component {
  state = {
    anchorEl: null,
  };

  handleClick = (event) => {
    this.setState({ anchorEl: event.currentTarget });
  };

  handleClose = (ev) => {
    const { onClick } = this.props;
    if (onClick) {
      onClick(ev.target.value);
    }
    this.setState({ anchorEl: null });
  };

  render() {
    const { data } = this.props;
    const { anchorEl } = this.state;

    return (
      <div>
        <Button
          aria-owns={anchorEl ? 'simple-menu' : null}
          aria-haspopup="true"
          onClick={this.handleClick}
        >
          Open Menu
        </Button>
        <Menu
          id="simple-menu"
          anchorEl={anchorEl}
          open={Boolean(anchorEl)}
          onClose={this.handleClose}
        >
          {data
            ? data.map(({ value, id }, idx) => {
                return (
                  <MenuItem key={idx} onClick={this.handleClose}>
                    {value}
                  </MenuItem>
                );
              })
            : null}
        </Menu>
      </div>
    );
  }
}

SimpleMenu.propTypes = {
  classes: PropTypes.object.isRequired,
};

SimpleMenu = withStyles(styles)(SimpleMenu);
export default SimpleMenu;
