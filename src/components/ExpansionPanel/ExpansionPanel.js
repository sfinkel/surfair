import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import Typography from '@material-ui/core/Typography';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import styles from './styles';
class ControlledExpansionPanels extends React.Component {
  state = {
    expanded: null,
  };

  handleChange = (panel) => (event, expanded) => {
    this.setState({
      expanded: expanded ? panel : false,
    });
  };

  render() {
    const { classes, data = [] } = this.props;
    const { expanded } = this.state;

    return (
      <div className={classes.root}>
        {data.map(({ id, heading, secondaryHeading, description }, idx) => {
          const key = id || idx;
          return (
            <ExpansionPanel
              key={key}
              expanded={expanded === `panel${key}`}
              onChange={this.handleChange(`panel${key}`)}
            >
              <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
                {heading ? (
                  <Typography className={classes.heading}>{heading}</Typography>
                ) : null}
                {secondaryHeading ? (
                  <Typography className={classes.secondaryHeading}>
                    {secondaryHeading}
                  </Typography>
                ) : null}
              </ExpansionPanelSummary>
              {description ? (
                <ExpansionPanelDetails>
                  <Typography>{description}</Typography>
                </ExpansionPanelDetails>
              ) : null}
            </ExpansionPanel>
          );
        })}
      </div>
    );
  }
}

ControlledExpansionPanels.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(ControlledExpansionPanels);
