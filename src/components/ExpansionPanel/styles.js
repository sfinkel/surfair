export default (theme) => ({
  root: {
    width: '100%',
  },
  heading: {
    fontSize: theme.typography.pxToRem(15),
    flexBasis: '33.33%',
    flexShrink: 0,
  },
  secondaryHeading: {
    padding: '0px',
    fontSize: theme.typography.pxToRem(15),
    color: theme.palette.text.secondary,
  },
});
