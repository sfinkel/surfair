import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import { Grid } from '@material-ui/core';

const Item = ({
  classes: { button, input },
  disabled,
  type,
  id,
  name,
  onClick,
}) => (
  <Grid container>
    <Grid item xs={6} />
  </Grid>
);

const styles = (theme) => ({
  button: {
    margin: 12,
    boxShadow: 'none',
  },
  input: {
    width: `calc(100% - ${16}px)`,
  },
});

export default withStyles(styles)(Item);
