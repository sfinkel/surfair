import React, { Component } from 'react';
import { Button, Grid } from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';
import { Item } from './';

class ItemList extends Component {
  handleItemAdd = () => {
    this.props.fields.push({});
  };

  handleItemDelete = (index) => () => {
    this.props.fields.remove(index);
  };

  render() {
    const {
      buttonText,
      classes: { addItemButton, section },
      disabled,
      fields,
    } = this.props;

    return (
      <Grid container className={section}>
        {fields.map((name, index) => (
          <Item
            id={index}
            disabled={disabled}
            key={index}
            name={name}
            onClick={this.handleItemDelete(index)}
            type="text"
          />
        ))}
        <Grid item xs={12}>
          <Button
            aria-label="add"
            className={addItemButton}
            disabled={disabled}
            size="small"
            onClick={this.handleItemAdd}
            variant="flat"
          >
            {buttonText}
          </Button>
        </Grid>
      </Grid>
    );
  }
}

const styles = (theme) => ({
  addItemButton: {
    backgroundColor: '#006EFF',
    borderColor: '#006EFF',
    borderRadius: 100,
    boxShadow: 'none',
    color: '#FFFFFF',
    marginTop: 12,
    '&:hover': {
      backgroundColor: '#0069d9',
      borderColor: '#0062cc',
    },
    '&:active': {
      boxShadow: 'none',
      backgroundColor: '#0062cc',
      borderColor: '#005cbf',
    },
    '&:focus': {
      boxShadow: '0 0 0 0.2rem rgba(0,123,255,.5)',
    },
  },
  section: {
    marginBottom: 12,
  },
});

export default withStyles(styles)(ItemList);
