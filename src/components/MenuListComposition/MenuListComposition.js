import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { Manager, Target, Popper } from 'react-popper';
import Button from '@material-ui/core/Button';
import ClickAwayListener from '@material-ui/core/ClickAwayListener';
import Collapse from '@material-ui/core/Collapse';
// import Grow from '@material-ui/core/Grow';
import Paper from '@material-ui/core/Paper';
import Portal from '@material-ui/core/Portal';
import MenuItem from '@material-ui/core/MenuItem';
import MenuList from '@material-ui/core/MenuList';
import { withStyles } from '@material-ui/core/styles';

const styles = (theme) => ({
  root: {
    display: 'flex',
  },
  paper: {
    marginRight: theme.spacing.unit * 2,
  },
  popperClose: {
    pointerEvents: 'none',
  },
});

class MenuListComposition extends React.Component {
  state = {
    open: false,
  };

  handleToggle = () => {
    this.setState((state) => ({ open: !state.open }));
  };

  handleClose = (event) => {
    if (this.target2.contains(event.target)) {
      return;
    }

    this.setState({ open: false });
  };

  render() {
    const {
      classes,
      data,
      openText = 'open',
      closeText = 'close',
    } = this.props;
    const { open } = this.state;

    return (
      <div className={classes.root}>
        {/* <Manager>
          <Target>
            <div
              ref={(node) => {
                this.target1 = node;
              }}
            >
              <Button
                aria-owns={open ? 'menu-list-grow' : null}
                aria-haspopup="true"
                onClick={this.handleToggle}
              >
                {openText}
              </Button>
            </div>
          </Target>
          <Popper
            placement="bottom-start"
            eventsEnabled={open}
            className={classNames({ [classes.popperClose]: !open })}
          >
            <ClickAwayListener onClickAway={this.handleClose}>
              <Grow
                in={open}
                id="menu-list-grow"
                style={{ transformOrigin: '0 0 0' }}
              >
                <Paper>
                  <MenuList role="menu">
                    {data
                      ? data.map(({ value, id }, idx) => {
                          return (
                            <MenuItem
                              key={`menu-list-grow-${idx}`}
                              onClick={this.handleClose}
                            >
                              {value}
                            </MenuItem>
                          );
                        })
                      : null}
                  </MenuList>
                </Paper>
              </Grow>
            </ClickAwayListener>
          </Popper>
        </Manager> */}
        <Manager>
          <Target>
            <div
              ref={(node) => {
                this.target2 = node;
              }}
            >
              <Button
                aria-owns={open ? 'menu-list-collapse' : null}
                aria-haspopup="true"
                onClick={this.handleToggle}
              >
                {!open ? openText : closeText}
              </Button>
            </div>
          </Target>
          <Portal>
            <Popper
              placement="bottom"
              eventsEnabled={open}
              className={classNames({ [classes.popperClose]: !open })}
            >
              <ClickAwayListener onClickAway={this.handleClose}>
                <Collapse
                  in={open}
                  id="menu-list-collapse"
                  style={{ transformOrigin: '0 0 0' }}
                >
                  <Paper style={{ margin: 3 }}>
                    <MenuList role="menu">
                      {data
                        ? data.map(({ value, id }, idx) => {
                            return (
                              <MenuItem
                                key={`menu-list-collapse-${idx}`}
                                onClick={this.handleClose}
                              >
                                {value}
                              </MenuItem>
                            );
                          })
                        : null}
                    </MenuList>
                  </Paper>
                </Collapse>
              </ClickAwayListener>
            </Popper>
          </Portal>
        </Manager>
      </div>
    );
  }
}

MenuListComposition.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(MenuListComposition);
