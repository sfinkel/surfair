import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import { Grid } from '@material-ui/core';

const Footer = ({ classes }) => (
  <Grid container className={classes.root}>
    <Grid item xs={3} className={classes.color}>
      A
    </Grid>
    <Grid item xs={3} className={classes.color}>
      Demo
    </Grid>
    <Grid item xs={3} className={classes.color}>
      App
    </Grid>
    <Grid item xs={3} className={classes.color}>
      Copyright &copy; 2018 All Rights Reserved
    </Grid>
  </Grid>
);

//TODO export these styles
const styles = {
  color: 'black',
  root: {
    position: 'absolute',
    bottom: 0,
  },
};

export default withStyles(styles)(Footer);
