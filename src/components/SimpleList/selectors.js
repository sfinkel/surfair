import { createSelector } from 'reselect';

const getAppData = (state = {}, props = {}) => {
  return props.data;
};

export const listData = createSelector(getAppData, (data) => data);

export const makeListData = () => {
  return createSelector([getAppData], (data) => data);
};
