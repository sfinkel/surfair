import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import ChevRight from '@material-ui/icons/ChevronRight';
import { map } from 'lodash';
import { connect } from 'react-redux';

const styles = (theme) => ({
  root: {
    width: '100%',
    maxWidth: 360,
    backgroundColor: theme.palette.background.paper,
  },
});

class SimpleList extends React.Component {
  render() {
    const { classes, data, onClick } = this.props;
    return (
      <div className={classes.root}>
        <List component="nav">
          {map(data, (value) => {
            console.log('value :', value);
            return (
              <ListItem button key={value} onClick={(ev) => onClick(ev, value)}>
                <ListItemIcon>
                  <ChevRight />
                </ListItemIcon>
                <ListItemText primary={value} />
              </ListItem>
            );
          })}
        </List>
      </div>
    );
  }
}

SimpleList.propTypes = {
  classes: PropTypes.object.isRequired,
};

SimpleList = withStyles(styles)(SimpleList);
export default connect(
  (state) => state,
  null,
)(SimpleList);
