import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import { Grid } from '@material-ui/core';

export class Layout extends Component {
  render() {
    const { children, classes } = this.props;
    return (
      <Grid
        container
        justify={'center'}
        direction={'row'}
        className={classes.root}
      >
        {children}
      </Grid>
    );
  }
}

//TODO export these styles
const styles = ({ spacing: { unit }, maxWidth }) => ({
  root: {
    margin: '0 auto',
    maxWidth,
    //padding: '56px 0 0 0',
  },
});

export default withStyles(styles)(Layout);
