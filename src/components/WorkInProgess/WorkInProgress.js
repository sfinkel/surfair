import React, { Component } from 'react';

class WorkInProgress extends Component {
  render() {
    return (
      <h1>
        <span role="img" aria-label="fire">
          🔥🔥🔥
        </span>
        Work in Progress for this page.
        <span role="img" aria-label="fire">
          🔥🔥🔥
        </span>
      </h1>
    );
  }
}

export default WorkInProgress;
