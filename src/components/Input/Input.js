import React from 'react';
import { defaultTo } from 'lodash';
import { TextField } from '@material-ui/core';

const Input = ({ helperText, id, input, meta, type, ...rest }) => {
  const { touched, error } = defaultTo(meta, {});

  return (
    <TextField
      id={id}
      type={type}
      helperText={touched && Boolean(error) ? error : helperText}
      error={touched && Boolean(error)}
      {...input}
      {...rest}
    />
  );
};

export default Input;
