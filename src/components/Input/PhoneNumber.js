import React from 'react';
import Input from './Input';
import NumberFormat from 'react-number-format';

const PhoneNumber = (props) => (
  <Input
    {...props}
    type="tel"
    InputProps={{
      inputComponent: ({ inputRef, onChange, onFocus, ...rest }) => (
        <NumberFormat {...rest} format="###-###-####" mask="_" ref={inputRef} />
      ),
    }}
  />
);

export default PhoneNumber;
