const initialState = {
  app: {
    quotes: [],
    symbols: {},
  },
  routing: {},
};

export default initialState;
