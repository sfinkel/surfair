import axios from 'axios';
//import response from './response.json';

const baseUrl = `https://forex.1forge.com/1.0.3`;
// TODO remove api key to env specific
const apiKey = `wLgLNY6dMO7TiUUnk5vIeMsfRVFETtx7`;
const symbolsUrl = `${baseUrl}/symbols?api_key=${apiKey}`;
const quotesUrl = `${baseUrl}/quotes?api_key=${apiKey}`;

export async function fetchInitialData() {
  const { data } = await axios.get(symbolsUrl, {
    responseType: 'json',
  });

  return data;
}

export async function fetchQuoteData(quotes) {
  const formedUrl =
    typeof quotes !== 'string'
      ? `${quotesUrl}&pairs=${quotes.join('')}`
      : `${quotesUrl}&pairs=${quotes}`;
  const { data } = await axios.get(formedUrl, {
    responseType: 'json',
  });
  return data;
}
